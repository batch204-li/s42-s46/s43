const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require('../auth.js');

// Create a product -Admin
router.post("/", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	console.log(isAdmin);

	if(isAdmin) {
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
	

});

// Get All Product
router.get("/all", (req, res) => {

	productController.getAllProducts().then(resultFromController => res.send(resultFromController))

});

// Get All Active Products
router.get("/", (req, res) => {

	productController.getAllActive().then(resultFromController => res.send(resultFromController));

});

// Get Specific Product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId)
	console.log(req.params)

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));

});

// Update a Product - Admin
router.put("/:productId", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	console.log(isAdmin);

	if(isAdmin) {
		productController.updateProducts(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}

});

// Archive Product - Admin
router.put('/:productId/archive', auth.verify, (req, res) => {

	console.log(req.params)

	const data = {
		courseId : req.params.courseId,
		payload : auth.decode(req.headers.authorization).isAdmin
	}

	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	console.log(isAdmin);

	if(isAdmin) {
		productController.updateProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
});



module.exports = router;




