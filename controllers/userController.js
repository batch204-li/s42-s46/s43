const User = require("../models/User");
const Products = require("../models/Products");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//Check if email exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0) {
			return true
		} else {
			return false
		}

	})
}



//Controller for User Registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({

		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});


	return newUser.save().then((user, error) => {

		if(error) {
			return false
		} else {
			return true
		}

	})
}


//User Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {
			
			return false

		} else {

		
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
		
			if(isPasswordCorrect) {

				console.log(result)

				return { access: auth.createAccessToken(result)}

			} else {

				return false
			}
		}

	})
}

// Retrieve user details
module.exports.getProfile = (data) => {

	return User.findById(data.id).then(result => {

		result.password = "";

		return result;

	});

};



module.exports.enroll = async (data) => {

	if (data.isAdmin === true) {
		return false
	} else {

		let isUserUpdated = await User.findById(data.userId).then( user => {

		
			product.orders.push({productId: data.productId});

			
			return user.save().then( (user, error) => {

				if(error) {
					return false
				} else {
					return true
				
				}
		
			});
		});


		let isProductUpdated = await Products.findById(data.courseId).then(course => {

				product.orders.push({userId: data.userId})

				return product.save().then((course, error) => {

					if(error) {
						return false

					} else {
						
						return true
					}
				});

		});

		if(isUserUpdated && isProductUpdated) {

			
			return true

		} else {

			return false
		}

	}

};