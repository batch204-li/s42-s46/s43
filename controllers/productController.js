const Products = require("../models/Products");
const User = require("../models/User");


//Create a New Product
module.exports.addProduct = (reqBody) => {

	let newProducts = new Products({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newProducts.save().then( (product, error) => {

		if(error) {

			return false
		
		} else {

			return true
		}

	})
}


module.exports.getAllProducts = () => {

	return Products.find({}).then(result => {

		return result;
	
	})

}

//Controller for retrieving all ACTIVE products
module.exports.getAllActive = () => {

	return Products.find({isActive: true}).then( result => {
		return result;
	})

}



//Retrieving a specific product
module.exports.getProduct = (reqParams) => {


	return Products.findById(reqParams.productId).then(result => {
		
		return result
	
	})

}



//Updating product
module.exports.updateProducts = (reqParams, reqBody, data) => {

	return User.findById(data.id).then(result => {
		console.log(result)

		if(result.isAdmin === true) {

			let updatedProduct = {
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			};

			return Products.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {

				
				if(error) {

					return false

				} else {

					return true
				}
			})

		} else {

			return false
		}

	})


}


// Archive A Product
module.exports.archiveCourse = (data, reqBody) => {

	return Product.findById(data.productId).then(result => {

		if (data.payload === true) {

			let updateActiveField = {
				isActive: reqBody.isActive
			}

			return Product.findByIdAndUpdate(result._id, updateActiveField).then((course, err) => {

					if(err) {
					
						return false
					
					}  else {

						return 
					}
			})
			 
		} else {

			return false
		} 
	})

}
